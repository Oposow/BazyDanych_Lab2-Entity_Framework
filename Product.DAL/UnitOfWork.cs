﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Product.DAL
{
    public class UnitOfWork : IDisposable
    {
        class DBEntityEntryWrapper : IEntityEntry
        {
            public DBEntityEntryWrapper(DbEntityEntry entry)
            {
                this.entry = entry;
            }

            private readonly DbEntityEntry entry;

            public object Entity { get { return this.entry.Entity; } }

            public DbPropertyValues OriginalValues
            {
                get { return entry.OriginalValues; }
            }

            public DbPropertyValues CurrentValues
            {
                get { return entry.CurrentValues; }
            }

            public DbComplexPropertyEntry ComplexProperty(string propertyName)
            {
                return entry.ComplexProperty(propertyName);
            }

            public bool Modified
            {
                get { return entry != null && entry.State.HasFlag(EntityState.Modified); }
            }

            public bool Added
            {
                get { return entry != null && entry.State.HasFlag(EntityState.Added); }
            }

            public bool Deleted
            {
                get { return entry != null && entry.State.HasFlag(EntityState.Deleted); }
            }
        }


        private readonly DbContext _objectContext;
        private readonly Triggers.TriggersLibrary _triggersLibrary;

        public UnitOfWork(bool isLazyLoadingOn = true)
        {
            _objectContext = new ProductContext();
            _triggersLibrary = new Triggers.TriggersLibrary();

            _objectContext.Configuration.LazyLoadingEnabled = isLazyLoadingOn;
            _objectContext.Configuration.ProxyCreationEnabled = true;
            _objectContext.Configuration.AutoDetectChangesEnabled = true;
        }

        public void Dispose()
        {
            if (_objectContext != null)
            {
                _objectContext.Dispose();
            }
        }

        private void ExecuteTriggerBefore<T>(T entity, UnitOfWork unitOfWork, DbEntityEntry entry)
        {
            var iEntry = new DBEntityEntryWrapper(entry);
            var triggers = _triggersLibrary.GetTriggersBeforeChanges<T>();
            foreach (var trigger in triggers)
                trigger.Execute(entity, unitOfWork, iEntry);
        }

        private void ExecuteTriggerAfter<T>(T entity, UnitOfWork unitOfWork, DbEntityEntry entry)
        {
            var iEntry = new DBEntityEntryWrapper(entry);
            var triggers = _triggersLibrary.GetTriggersAfterChanges<T>();
            foreach (var trigger in triggers)
            {
                trigger.Execute(entity, unitOfWork, iEntry);
            }
        }

        static MethodInfo ExecuteTriggerBeforeMethodInfo = typeof(UnitOfWork)
            .GetMethod("ExecuteTriggerBefore", BindingFlags.NonPublic | BindingFlags.Instance);
        static MethodInfo ExecuteTriggerAfterMethodInfo = typeof(UnitOfWork)
            .GetMethod("ExecuteTriggerAfter", BindingFlags.NonPublic | BindingFlags.Instance);

        private void ExecuteTriggersBefore(IEnumerable<DbEntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                if (entry.State.HasFlag(EntityState.Added)
                    || entry.State.HasFlag(EntityState.Modified))
                {
                    Type type = entry.Entity.GetType();
                    MethodInfo generic = ExecuteTriggerBeforeMethodInfo.MakeGenericMethod(type);
                    generic.Invoke(this, new object[] { entry.Entity, this, entry });
                }
            }
        }

        private void ExecuteTriggersAfter(IEnumerable<DbEntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                {
                    Type type = entry.Entity.GetType();
                    MethodInfo generic = ExecuteTriggerAfterMethodInfo.MakeGenericMethod(type);
                    generic.Invoke(this, new object[] { entry.Entity, this, entry });
                }
            }
        }

        public void SaveChanges()
        {
            List<DbEntityEntry> x = null;

            x = _objectContext.ChangeTracker
                .Entries()
                .Where(e => e.State.HasFlag(EntityState.Added) 
                    || e.State.HasFlag(EntityState.Modified))
                .ToList();

            ExecuteTriggersBefore(x);

            _objectContext.SaveChanges();

            ExecuteTriggersAfter(x);

            _objectContext.SaveChanges();
        }

        public Repository<T> GetRepository<T>() where T : class
        {
            return new Repository<T>(_objectContext);
        }

        public DbContext GetContext()
        {
            return _objectContext;
        }
    }
}