namespace Product.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "Name", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Customers", "Address_Street", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "Address_City", c => c.String(nullable: false));
            AlterColumn("dbo.Customers", "Address_PostalCode", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.Orders", "CreationDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Orders", "ModificationDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "ModificationDate", c => c.DateTime());
            AlterColumn("dbo.Orders", "CreationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Address_PostalCode", c => c.String(maxLength: 6));
            AlterColumn("dbo.Customers", "Address_City", c => c.String());
            AlterColumn("dbo.Customers", "Address_Street", c => c.String());
            AlterColumn("dbo.Customers", "Name", c => c.String(maxLength: 64));
            AlterColumn("dbo.Products", "Name", c => c.String());
        }
    }
}
