// <auto-generated />
namespace Product.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class OrderHistory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(OrderHistory));
        
        string IMigrationMetadata.Id
        {
            get { return "201710291650417_OrderHistory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
