namespace Product.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CreationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "ModificationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ModificationDate");
            DropColumn("dbo.Orders", "CreationDate");
        }
    }
}
