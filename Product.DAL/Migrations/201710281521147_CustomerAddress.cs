namespace Product.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Address_Street", c => c.String());
            AddColumn("dbo.Customers", "Address_City", c => c.String());
            AddColumn("dbo.Customers", "Address_PostalCode", c => c.String());
            AddColumn("dbo.Orders", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Description");
            DropColumn("dbo.Customers", "Address_PostalCode");
            DropColumn("dbo.Customers", "Address_City");
            DropColumn("dbo.Customers", "Address_Street");
        }
    }
}
