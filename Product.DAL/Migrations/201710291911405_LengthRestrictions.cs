namespace Product.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LengthRestrictions : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "Address_PostalCode", c => c.String(maxLength: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "Address_PostalCode", c => c.String());
        }
    }
}
