﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Product.DAL.Entities
{
    public class Customer
    {
        public Customer()
        {
            Address = new Address();
        }

        public int Id { get; set; }
        [MaxLength(64)]
        [Required]
        public string Name { get; set; }
        public bool IsCompany { get; set; }

        [MaxLength(20)]
        public string NipOrPesel { get; set; }

        public Address Address { get; set; }
    }

    [ComplexType]
    public class Address
    {
        [Required]
        public string Street { get; set; }
        [Required]
        public string City { get; set; }
        [MaxLength(6)]
        [Required]
        public string PostalCode { get; set; }
    }
}
