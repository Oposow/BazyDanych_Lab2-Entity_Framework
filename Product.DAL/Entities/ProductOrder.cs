﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Product.DAL.Entities
{
    public class ProductOrder
    {
        [Key, Column(Order = 0)]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        [Key, Column(Order = 1)]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        public int Quantity { get; set; }
    }
}
