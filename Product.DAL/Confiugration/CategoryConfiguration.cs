﻿using System.Data.Entity.ModelConfiguration;

namespace Product.DAL.Confiugration
{
    public class CategoryConfiguration : EntityTypeConfiguration<Entities.Category>
    {
        public CategoryConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(40);
        }
    }
}
