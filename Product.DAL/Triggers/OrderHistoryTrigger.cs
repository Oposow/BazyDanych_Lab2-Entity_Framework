﻿using System;

namespace Product.DAL.Triggers
{
    public class OrderHistoryTrigger : ITriggerBefore<Entities.Order>
    {
        public void Execute(Entities.Order target, UnitOfWork iuow, IEntityEntry entry)
        {
            if (entry.Added)
                target.CreationDate = DateTime.Now;
            else if (entry.Modified)
                target.ModificationDate = DateTime.Now;
        }
    }
}
