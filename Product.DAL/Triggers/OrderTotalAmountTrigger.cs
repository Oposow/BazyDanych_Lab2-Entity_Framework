﻿using System.Linq;

namespace Product.DAL.Triggers
{
    public class OrderTotalAmountTrigger : ITriggerAfter<Entities.Order>
    {
        public void Execute(Entities.Order target, UnitOfWork iuow, IEntityEntry entry)
        {
            target.TotalValue = target.Products
                .Select(x => x.Quantity * x.Product.UnitPrice)
                .DefaultIfEmpty()
                .Sum();
        }
    }
}
