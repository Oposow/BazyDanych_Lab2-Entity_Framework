﻿using Product.DAL.Entities;
using System.Data.Entity;
using System;

namespace Product.DAL
{
    public class ProductContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Entities.Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Conventions
            modelBuilder.Properties<int>().Where(p => p.Name == "Id").Configure(p => p.IsKey());    // If columns has 'Id' name, change to 'Primary Key'
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
            #endregion

            #region Model Configuration
            modelBuilder.Configurations.AddFromAssembly(this.GetType().Assembly);
            #endregion
            base.OnModelCreating(modelBuilder);
        }

    }
}
