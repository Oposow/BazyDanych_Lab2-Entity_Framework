﻿using Microsoft.Practices.Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;
using System;
using Product.DAL.Entities;

namespace Product.Models
{
    public class OrderModel : BindableBase
    {
        private int _id;
        public int Id {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private DateTime _creationDate;
        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { SetProperty(ref _creationDate, value); }
        }

        private DateTime? _modificationDate;
        public DateTime? ModificationDate
        {
            get { return _modificationDate; }
            set { SetProperty(ref _modificationDate, value); }
        }

        private string _customerName;
        public string CustomerName
        {
            get { return _customerName; }
            set { SetProperty(ref _customerName, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _productsInfo;
        public string ProductsInfo
        {
            get { return _productsInfo; }
            set { SetProperty(ref _productsInfo, value); }
        }

        private decimal _totalAmount;
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { SetProperty(ref _totalAmount, value); }
        }

        private string _customerAddress;
        public string CustomerAddress
        {
            get { return _customerAddress; }
            set { SetProperty(ref _customerAddress, value); }
        }

        private Customer _customer { get; set; }
        public Customer Customer {
            get { return _customer; }
            set
            {
                CustomerName = value.Name;
                _customer = value;
                CustomerAddress = $"{_customer.Address.Street}, {_customer.Address.PostalCode} {_customer.Address.City}";
            }
        }
        public List<ProductOrder> Products { get; set; }

        public void RefreshProductsInfo()
        {
            ProductsInfo = String.Join(", ", Products.Select(x => $"{x.Product.Name} x{x.Quantity}"));
        }
    }
}
