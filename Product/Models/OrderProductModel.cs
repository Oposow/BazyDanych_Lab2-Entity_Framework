﻿using Microsoft.Practices.Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;
using System;
using Product.DAL.Entities;


namespace Product.Models
{
    public class OrderProductModel : BindableBase
    {
        private int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { SetProperty(ref _productId, value); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private int _quantity;
        public int Quantity
        {
            get { return _quantity; }
            set
            {
                SetProperty(ref _quantity, value);
                TotalValue = value * UnitPrice;
            }
        }

        private decimal _unitPrice;
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { SetProperty(ref _unitPrice, value); }
        }

        private decimal _totalValue;
        public decimal TotalValue
        {
            get { return _totalValue; }
            set { SetProperty(ref _totalValue, value); }
        }
    }
}
