﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System.Windows.Threading;
using Product.DAL.Entities;
using Product.DAL;
using System.Data.Entity;

namespace Product.Views
{
    public class ProductsViewModel : BindableBase
    {
        public ProductsViewModel()
        {
            unitOfWork = new UnitOfWork();

            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(OnCancel);
            AddItemCommand = new DelegateCommand(OnAdd);
            EditItemCommand = new DelegateCommand(OnEdit);

            ViewLoaded = new DelegateCommand(OnLoad);
            ViewUnloaded = new DelegateCommand(OnUnload);

            Products = new ObservableCollection<DAL.Entities.Product>();
            Categories = new ObservableCollection<Category>();

            filteringTimer.Tick += new EventHandler(OnFilter);
            filteringTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
        }

        #region commands
        public ICommand SaveCommand { get; set; }
        private void Save()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(EditModelName) || SelectedCategory == null)
                    throw new Exception();

                var productsRepo = unitOfWork.GetRepository<DAL.Entities.Product>();
                if (EditModelId == 0)
                {
                    productsRepo.Add(new DAL.Entities.Product()
                    {
                        Name = EditModelName,
                        Description = EditModelDescription,
                        UnitPrice = _editModelPrice,
                        Category = SelectedCategory
                    });
                }
                else
                {
                    SelectedProduct.Name = EditModelName;
                    SelectedProduct.Description = EditModelDescription;
                    SelectedProduct.UnitPrice = _editModelPrice;
                    SelectedProduct.Category = SelectedCategory;
                }
                unitOfWork.SaveChanges();

                Filter(FilterPattern);
                IsTableView = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't save changes. Please make sure, that all fields were entered correctly",
                    "Saving changes error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        public ICommand ViewLoaded { get; set; }
        private void OnLoad()
        {
            if (unitOfWork == null)
                unitOfWork = new UnitOfWork();
            IsTableView = true;
            Filter(null);
        }

        public ICommand ViewUnloaded { get; set; }
        private void OnUnload()
        {
            if (unitOfWork != null)
            {
                unitOfWork.Dispose();
                unitOfWork = null;
            }
        }

        public ICommand EditItemCommand { get; set; }
        private void OnEdit()
        {
            if (SelectedProduct == null)
                return;

            PrepareEditView(SelectedProduct);
        }

        public ICommand AddItemCommand { get; set; }
        private void OnAdd()
        {
            PrepareEditView(new DAL.Entities.Product());
        }

        public ICommand CancelCommand { get; set; }
        private void OnCancel()
        {
            IsTableView = true;
        }

        #endregion

        #region properties
        private UnitOfWork unitOfWork;
        DispatcherTimer filteringTimer = new DispatcherTimer();

        private string _filterPattern;
        public string FilterPattern
        {
            get { return _filterPattern; }
            set { SetProperty(ref _filterPattern, value); filteringTimer.Stop(); filteringTimer.Start(); }
        }

        private bool _isTableView;
        public bool IsTableView
        {
            get { return _isTableView; }
            set { SetProperty(ref _isTableView, value); }
        }

        public ObservableCollection<DAL.Entities.Product> Products { get; set; }
        public ObservableCollection<Category> Categories { get; set; }

        public DAL.Entities.Product SelectedProduct { get; set; }

        private string _editModelName;
        public string EditModelName
        {
            get { return _editModelName; }
            set { SetProperty(ref _editModelName, value); }
        }

        private string _editModelDescription;
        public string EditModelDescription
        {
            get { return _editModelDescription; }
            set { SetProperty(ref _editModelDescription, value); }
        }

        private decimal _editModelPrice;
        public string EditModelPrice
        {
            get { return _editModelPrice.ToString("0.00"); }
            set {
                decimal val = 0;
                if (Decimal.TryParse(value, out val))
                    SetProperty(ref _editModelPrice, val);
            }
        }

        public Category SelectedCategory { get; set; }
        private int EditModelId { get; set; }
        #endregion

        #region methods


        private void OnFilter(object sender, EventArgs e)
        {
            Filter(FilterPattern);
        }

        private void Filter(string pattern)
        {
            filteringTimer.Stop();
            var query = unitOfWork.GetRepository<DAL.Entities.Product>().AsQueryable();
            if (!String.IsNullOrWhiteSpace(pattern))
            {
                var words = pattern.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                query = (from p in query
                         where words.All(w => (p.Name + " " + p.Category.Name).Contains(w))
                         select p);
            }

            query = query.Include(x => x.Category);
            query = query.OrderBy(x => x.Name);

            Products.Clear();
            foreach (var p in query.ToList())
                Products.Add(p);
        }

        private void PrepareEditView(DAL.Entities.Product model)
        {
            IsTableView = false;

            EditModelPrice = model.UnitPrice.ToString();
            EditModelName = model.Name;
            EditModelDescription = model.Description;
            EditModelId = model.Id;

            var categories = unitOfWork.GetRepository<Category>().AsQueryable().ToList();
            Categories.Clear();
            foreach (var c in categories)
                Categories.Add(c);

            SelectedCategory = Categories.FirstOrDefault(x => x.Id == model.CategoryId);
            OnPropertyChanged("SelectedCategory");
        }
        #endregion
    }
}