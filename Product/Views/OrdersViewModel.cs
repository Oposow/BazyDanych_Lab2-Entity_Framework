﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System.Windows.Threading;
using Product.DAL.Entities;
using Product.DAL;
using Product.Models;

namespace Product.Views
{
    public class OrdersViewModel : BindableBase, INotifyPropertyChanged
    {
        public OrdersViewModel()
        {
            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(OnCancel);
            AddItemCommand = new DelegateCommand(OnAdd);
            EditItemCommand = new DelegateCommand(OnEdit);

            ViewLoaded = new DelegateCommand(OnLoad);
            ViewUnloaded = new DelegateCommand(OnUnload);

            Orders = new ObservableCollection<OrderModel>();
            Customers = new ObservableCollection<Customer>();
            Products = new ObservableCollection<OrderProductModel>();

            filteringTimer.Tick += new EventHandler(OnFilter);
            filteringTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
        }

        #region commands
        public ICommand SaveCommand { get; set; }
        private void Save()
        {
            try
            {
                var selectedProducts = Products.Where(x => x.Quantity > 0);
                if (!selectedProducts.Any())
                {
                    MessageBox.Show("Can't save order without products", "No products selected", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (SelectedCustomer == null || SelectedCustomer.Id == 0)
                {
                    MessageBox.Show("No choosen customer", "No customer", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                using (var unitOfWork = new UnitOfWork())
                {
                    Order order;
                    var repo = unitOfWork.GetRepository<Order>();
                    if (EditModelId != 0)
                        order = repo.Find(EditModelId);
                    else
                        order = new Order();

                    order.Customer = unitOfWork.GetRepository<Customer>().Find(SelectedCustomer.Id);
                    order.Description = EditModelDescription;

                    var selectedIds = selectedProducts.Select(x => x.ProductId).ToArray();
                    var productsDb = unitOfWork.GetRepository<DAL.Entities.Product>().AsQueryable().Where(x => selectedIds.Contains(x.Id));

                    order.Products.Clear();
                    foreach (var p in productsDb)
                        if (!order.Products.Any(x => x.ProductId == p.Id))
                            order.Products.Add(new ProductOrder()
                            {
                                Product = p,
                                ProductId = p.Id,
                                Quantity = selectedProducts.First(x => x.ProductId == p.Id).Quantity
                            });

                    if (EditModelId == 0)
                        repo.Add(order);

                    unitOfWork.SaveChanges();
                }

                Filter(FilterPattern);
                IsTableView = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't save changes. Please make sure, that all fields were entered correctly",
                    "Saving changes error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        public ICommand ViewLoaded { get; set; }
        private void OnLoad()
        {
            IsTableView = true;
            Filter(null);
        }

        public ICommand ViewUnloaded { get; set; }
        private void OnUnload()
        {
        }

        public ICommand EditItemCommand { get; set; }
        private void OnEdit()
        {
            if (SelectedOrder == null)
                return;
            PrepareEditView(SelectedOrder);
        }

        public ICommand AddItemCommand { get; set; }
        private void OnAdd()
        {
            PrepareEditView(new OrderModel());
        }

        public ICommand CancelCommand { get; set; }
        private void OnCancel()
        {
            IsTableView = true;
        }

        #endregion

        #region properties
        DispatcherTimer filteringTimer = new DispatcherTimer();

        private string _filterPattern;
        public string FilterPattern
        {
            get { return _filterPattern; }
            set { SetProperty(ref _filterPattern, value); filteringTimer.Stop(); filteringTimer.Start(); }
        }

        private bool _isTableView;
        public bool IsTableView
        {
            get { return _isTableView; }
            set { SetProperty(ref _isTableView, value); }
        }

        public ObservableCollection<OrderModel> Orders { get; set; }
        public OrderModel SelectedOrder { get; set; }
        #endregion

        #region methods
        private void OnFilter(object sender, EventArgs e)
        {
            Filter(FilterPattern);
        }

        private void Filter(string pattern)
        {
            filteringTimer.Stop();
            using (var unitOfWork = new UnitOfWork())
            {
                var query = unitOfWork.GetRepository<Order>().AsNoTracking();
                if (!String.IsNullOrWhiteSpace(pattern))
                {
                    var words = pattern.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    query = query.Where(x => words.All(w => x.Products.Any(p => p.Product.Name.Contains(w)) 
                        || (x.Customer.Name + " " + x.Customer.Address.City + " " + x.Customer.Address.Street).Contains(w)));
                }

                query = query.OrderByDescending(x => x.CreationDate);

                var result = query.Select(x => new OrderModel()
                {
                    CreationDate = x.CreationDate,
                    Customer = x.Customer,
                    Description = x.Description,
                    Id = x.Id,
                    ModificationDate = x.ModificationDate,
                    TotalAmount = x.TotalValue,
                    Products = x.Products.ToList()
                }).ToList();

                Orders.Clear();
                result.ForEach(x => { x.RefreshProductsInfo(); Orders.Add(x); });
            }
        }

        private void PrepareEditView(OrderModel model)
        {
            EditModelDescription = model.Description;
            _editModelCreationDate = model.CreationDate;
            _editModelModificationDate = model.ModificationDate;
            EditModelId = model.Id;
            
            using (var unitOfWork = new UnitOfWork())
            {
                var customers = unitOfWork.GetRepository<Customer>().AsNoTracking().ToList();
                Customers.Clear();
                foreach (var c in customers)
                    Customers.Add(c);

                var products = unitOfWork.GetRepository<DAL.Entities.Product>()
                    .AsNoTracking()
                    .Select(p => new OrderProductModel()
                    {
                        ProductId = p.Id,
                        Name = p.Name,
                        Quantity = 0,
                        TotalValue = 0,
                        UnitPrice = p.UnitPrice
                    })
                    .ToList();

                if (model.Id != 0)
                {
                    // Deffered execution - spada wydajność - tylko ze względu na wymagania zadania
                    var oldProducts = unitOfWork.GetRepository<Order>()
                        .Find(model.Id)
                        .Products.Select(x => new
                        {
                            ProductId = x.ProductId,
                            Quantity = x.Quantity
                        });

                    foreach (var p in oldProducts)
                        products.First(x => x.ProductId == p.ProductId).Quantity = p.Quantity;
                }

                products = products.OrderByDescending(x => x.TotalValue).ThenBy(x => x.Name).ToList();
                Products.Clear();
                foreach (var p in products)
                    Products.Add(p);

                SelectedCustomer = Customers.FirstOrDefault(x => x.Id == model.Customer?.Id);
            }
            IsTableView = false;
        }
        #endregion

        #region edition
        public ObservableCollection<OrderProductModel> Products { get; set; }
        public ObservableCollection<Customer> Customers { get; set; }

        private Customer _selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                if (value == null)
                    return;
                var addr = value.Address;
                SelectedCustomerAddress = $"{addr.Street}, {addr.PostalCode} {addr.City}";
                OnPropertyChanged("SelectedCustomer");
            }
        }

        private string _editModelDescription;
        public string EditModelDescription
        {
            get { return _editModelDescription; }
            set { SetProperty(ref _editModelDescription, value); }
        }

        private string _selectedCustomerAddress;
        public string SelectedCustomerAddress
        {
            get { return _selectedCustomerAddress; }
            set { SetProperty(ref _selectedCustomerAddress, value); }
        }

        private string _editModelCreationDateStr;
        private DateTime? _editModelCreationDate
        {
            set { EditModelCreationDateStr = value != new DateTime() ? value.Value.ToString("yyyy-MM-dd hh:mm") : "-"; }
        }
        public string EditModelCreationDateStr
        {
            get { return _editModelCreationDateStr; }
            set { SetProperty(ref _editModelCreationDateStr, value); }
        }

        private string _editModelModificationDateStr;
        private DateTime? _editModelModificationDate
        {
            set { EditModelModificationDateStr = value.HasValue ? value.Value.ToString("yyyy-MM-dd hh:mm") : "-"; }
        }
        public string EditModelModificationDateStr
        {
            get { return _editModelModificationDateStr; }
            set { SetProperty(ref _editModelModificationDateStr, value); }
        }

        private int EditModelId { get; set; }
        #endregion
    }
}